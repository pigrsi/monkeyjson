var data = [];
var selectedObject;
var currentAnimation;
var fileName;
var frames = [];
var frameIndex = 0;
var image = null;

function onFileLoad(elementId, event) {
    document.getElementById(elementId).innerText = event.target.result;
}

function onChooseFile(event, onLoadFileHandler) {
    if (typeof window.FileReader !== 'function')
        throw ("The file API isn't supported on this browser.");
    let input = event.target;
    if (!input)
        throw ("The browser does not properly implement the event object");
    if (!input.files)
        throw ("This browser does not support the `files` property of the file input.");
    if (!input.files[0])
        return undefined;
    let file = input.files[0];
    let fr = new FileReader();
    fr.onload = onLoadFileHandler;
    fr.readAsText(file);
	fileName = file.name;
	document.getElementById("middleBox").hidden = false;
}

function downloadFile() {
    var a = document.createElement("a");
    var file = new Blob([JSON.stringify(data)], {type: "application/json"});
    a.href = URL.createObjectURL(file);
    a.download = "animations.json";
    a.click();
}

function readJsonContents() {
	data = JSON.parse(document.getElementById("contents").innerText);
	for (let i = 0; i < data.length; ++i) {
		addObjectToList(data[i].name);
	}
}

function addObjectToList(name) {
	let list = document.getElementById("objectList");
	let div = document.createElement('div');
	div.classList.add("objectName");
	
	let link = document.createElement('a');
	link.href = "";
	link.innerText = name;
	link.addEventListener("click", function(e) {
	  e.preventDefault();
	  changeItemSelected(name);
	});
	
	div.appendChild(link);
	list.appendChild(div);
	
	let addNewObjectDiv = document.getElementById("addObjectDiv");
	list.removeChild(addNewObjectDiv);
	list.appendChild(addNewObjectDiv);
	
	list.scrollTo(0, list.scrollHeight);
}

function onKeyPress(event) {
	if (event.which != 13 && event.keyCode != 13) return true;
	let textBox = document.getElementById("addObjectBox");
	if (textBox.value) {
		addObjectToList(textBox.value);
		changeItemSelected(textBox.value);
	}
	textBox.value = "";
	return false;
}

function updateSprite() {
	if (!image || !selectedObject || !image.complete) {
		setTimeout(updateSprite, 250);
		return;
	}
	let spriteWidth = document.getElementById("spriteWidth").value;
	let spriteHeight = document.getElementById("spriteHeight").value;
	let frameRate = document.getElementById("frameRate").value;
	if (!spriteWidth || !spriteHeight || !frameRate) {
		setTimeout(updateSprite, 250)
		return;
	}
	// get frame position
	let frame = frames[frameIndex];
	let numFramesOnRow = image.naturalWidth / spriteWidth;
	let numFramesOnColumn = image.naturalHeight / spriteHeight;
	let sourceX = (frame % numFramesOnRow) * spriteWidth;
	let sourceY = (frame % numFramesOnColumn) * spriteHeight;
	image.draw(sourceX, sourceY, spriteWidth, spriteHeight);
	setTimeout(updateSprite, 1000/frameRate);
	frameIndex = (frameIndex + 1) % frames.length;
}
updateSprite();

function setSpritesheetFilename(filename) {
	if (!filename) return;
	if (!filename.includes('.png')) return;
	if (filename.includes("\\")) {
		filename = filename.split("\\");
		filename = filename[filename.length-1];
	}
	document.getElementById("spritesheetLoaderButton").value = "Spritesheet file: " + filename;
	loadSpritesheetFromPilefight(filename);
	if (selectedObject) {
		selectedObject.spritesheet = filename;
	}
}

function loadSpritesheetFromPilefight(filename) {
	image = new Image();
	
	let canvas = document.getElementById("animCanvas");
	let context = canvas.getContext('2d');

	image.draw = function (sx, sy, spriteWidth, spriteHeight) {
					context.clearRect(0, 0, canvas.width, canvas.height);
                    context.drawImage(image, sx, sy, spriteWidth, spriteHeight, canvas.width/2-spriteWidth/2, canvas.height/2-spriteHeight/2, spriteWidth, spriteHeight);
                };
				
	image.src = "../pilefight/client/game/assets/images/" + filename;
}

function changeItemSelected(itemName) {
	// Create item if it doesn't exist
	let object;
	for (let i = 0; i < data.length; ++i) {
		if (data[i].name === itemName) {
			object = data[i];
			break;
		}
	}
	
	if (!object)
		object = makeNewObject(itemName);
	
	if (image)
		image.draw(0, 0, 0, 0);
	
	selectedObject = object;
		
	if (object.animations.length === 0)
		createNewAnimationForObject("force", "idle");
	
	if (object.spritesheet)
		setSpritesheetFilename(object.spritesheet);
	
	document.getElementById("spritesheetLoaderButton").value = object.spritesheet ? "Spritesheet file: " + object.spritesheet : "Please load spritesheet!";
	
	document.getElementById("objectWindow").hidden = false;
	
	document.getElementById("itemTitle").innerText = itemName;
	document.getElementById("newAnimationBox").value = "";
	document.getElementById("newAnimationBox").placeholder = "Enter new animation name and hit enter";
	
	setupDropdown(object);
	
	updateAnimationParams(object.animations[0] ? object.animations[0] : {});
	
	// Object params
	document.getElementById("spriteWidth").value = object.frameWidth;
	document.getElementById("spriteHeight").value = object.frameHeight;
}

function changeSelectedAnimation(value) {
	if (!selectedAnimation) return true;
	for (let i = 0; i < selectedObject.animations.length; ++i) {
		if (selectedObject.animations[i].name === value) {
			updateAnimationParams(selectedObject.animations[i]);
			return false;
		}
	}
}

function setupDropdown(object) {
	// Setup dropdown box
	let dropdown = document.getElementById("selectedAnimation");
	for (let i = 0; i < dropdown.options.length; ++i)
		dropdown.options.length = 0;
	
	let animations = object.animations;
	for (let i = 0; i < animations.length; ++i) {
		addAnimationToList(animations[i]);
	}
}

function createNewAnimationForObject(event, name) {
	if (event != "force" && event.which != 13 && event.keyCode != 13) return true;
	if (!name) return false;
	let anim = makeNewAnimation(name);
	selectedObject.animations.push(anim);
	addAnimationToList(anim);
	document.getElementById("newAnimationBox").placeholder = "Animation added!";
	document.getElementById("newAnimationBox").value = "";
	document.getElementById("selectedAnimation").value = name;
	changeSelectedAnimation(name);
}

function addAnimationToList(anim) {
	let option = document.createElement('option');
	option.value = anim.name;
	option.innerText = anim.name;
	document.getElementById("selectedAnimation").appendChild(option);
}

function updateAnimationParams(anim) {
	currentAnimation = anim;
	document.getElementById("frameRate").value = anim.frameRate != undefined ? anim.frameRate : "";
	document.getElementById("startFrame").value = anim.start != undefined ? anim.start : "";
	document.getElementById("endFrame").value = anim.end != undefined ? anim.end: "";
	document.getElementById("frames").value = anim.frames != undefined ? anim.frames: "";
	document.getElementById("numRepeats").value = anim.repeat != undefined ? anim.repeat: "";
	document.getElementById("yoyo").checked = anim.yoyo != undefined ? anim.yoyo: false;
	
	frames = document.getElementById("frames").value;
	if (frames) {
		frames = frames.split(',');
		for (let i = 0; i < frames.length; ++i) {
			frames[i] = parseInt(frames[i]);
		}
	} else {
		let start = parseInt(document.getElementById("startFrame").value);
		let end = parseInt(document.getElementById("endFrame").value);
		frames = [];
		for (let i = start; i <= end; ++i) 
			frames.push(i);
	}
	
	if (anim.yoyo) {
		let reverseFrames = frames.slice().reverse();
		reverseFrames.shift();
		reverseFrames.pop();
		frames = frames.concat(reverseFrames);
	}
	
	frameIndex = 0;
}

function deleteCurrentItem() {
	if (!selectedObject) return;
	let index = data.indexOf(selectedObject);
	if (index > -1)
		data.splice(index, 1);
	
	// Remove from list
	let list = document.getElementById("objectList");
	let listElements = document.getElementsByClassName("objectName");
	for (var i = 0; i < listElements.length; ++i) {
		if (listElements[i].innerText === selectedObject.name)
			list.removeChild(listElements[i]);
	}
	
	selectedObject = null;
	document.getElementById("objectWindow").hidden = true;
}

function deleteCurrentAnimation() {
	if (!selectedObject || !currentAnimation) return;
	if (selectedObject.animations.length < 2) {
		alert("not allowed to have 0 animations. add one then delete this one");
		return;
	}
	let animations = selectedObject.animations;
	for (let i = 0; i < animations.length; ++i) {
		if (currentAnimation === animations[i]) {
			animations.splice(i, 1);
		}
	}
	
	changeSelectedAnimation(animations[0].name);
	setupDropdown(selectedObject);
}

function makeNewObject(name) {
	let ob = {
		name: name,
		spritesheet: "",
		frameWidth: 48,
		frameHeight: 48,
		animations: [],
	};
	data.push(ob);
	return ob;
}

function changeValueInObjectData(property, value) {
	if (!selectedObject) return;
	selectedObject[property] = value;
}

function changeValueInAnimationData(property, value) {
	if (!selectedObject || !currentAnimation) return;
	currentAnimation[property] = value;
	updateAnimationParams(currentAnimation);
}

function makeNewAnimation(name) {
	let animOb = {
		name: name,
		frameRate: 10,
		repeat: -1,
		yoyo: false
	};
	return animOb;
}

function hexToBase64(str) {
    return btoa(String.fromCharCode.apply(null, str.replace(/\r|\n/g, "").replace(/([\da-fA-F]{2}) ?/g, "0x$1 ").replace(/ +$/, "").split(" ")));
}

function toInt(val) {
	
}








